package com.jobapp.abdelkader.jobapp;

import android.app.Application;
import android.content.Context;

import com.jobapp.abdelkader.jobapp.dagger.componentes.DaggerJobFeedViewModelComponent;
import com.jobapp.abdelkader.jobapp.dagger.componentes.JobFeedViewModelComponent;
import com.jobapp.abdelkader.jobapp.dagger.modules.JobFeedModelViewModule;

/**
 * Created by user on 15/02/2017.
 */

public class JobApp extends Application {
private static Context context;
    JobFeedViewModelComponent jobFeedViewModelComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        jobFeedViewModelComponent = DaggerJobFeedViewModelComponent.builder()
                .jobFeedModelViewModule(new JobFeedModelViewModule())
                .build();
        context=this;

    }

    public JobFeedViewModelComponent getJobFeedViewModelComponent() {
        return jobFeedViewModelComponent;
    }

    public static Context getContext() {
        return context;
    }
}
