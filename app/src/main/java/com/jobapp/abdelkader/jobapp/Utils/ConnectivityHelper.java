package com.jobapp.abdelkader.jobapp.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.jobapp.abdelkader.jobapp.JobApp;

/**
 * Created by user on 15/02/2017.
 */

public class ConnectivityHelper {
    public static boolean isConnectedToInternet(){
        ConnectivityManager cm =
                (ConnectivityManager) JobApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
