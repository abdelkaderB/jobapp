package com.jobapp.abdelkader.jobapp.dagger.componentes;

import com.jobapp.abdelkader.jobapp.dagger.modules.JobFeedModelViewModule;
import com.jobapp.abdelkader.jobapp.jobfeed.JobFeedActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by user on 24/02/2017.
 */
@Singleton
@Component(modules={JobFeedModelViewModule.class})
public interface JobFeedViewModelComponent {
    void inject(JobFeedActivity jobFeedActivity);
}
