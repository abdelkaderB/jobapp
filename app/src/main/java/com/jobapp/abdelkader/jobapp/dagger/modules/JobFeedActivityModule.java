package com.jobapp.abdelkader.jobapp.dagger.modules;

import com.jobapp.abdelkader.jobapp.data.model.OfferLine;
import com.jobapp.abdelkader.jobapp.data.repository.FeedRepository;
import com.jobapp.abdelkader.jobapp.data.repository.OfferFeedRepository;
import com.jobapp.abdelkader.jobapp.jobfeed.JobFeedActivity;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user on 28/02/2017.
 */
@Module
public class JobFeedActivityModule {
    JobFeedActivity jobFeedActivity;

    public JobFeedActivityModule(JobFeedActivity jobFeedActivity) {
        this.jobFeedActivity = jobFeedActivity;
    }

    @Provides
    @Singleton
    JobFeedActivity providesJobFeedActivity(){
        return jobFeedActivity;
    }
}
