package com.jobapp.abdelkader.jobapp.dagger.modules;

import com.jobapp.abdelkader.jobapp.data.model.CandidateLine;
import com.jobapp.abdelkader.jobapp.data.model.OfferLine;
import com.jobapp.abdelkader.jobapp.data.repository.CandidateFeedRepository;
import com.jobapp.abdelkader.jobapp.data.repository.FeedRepository;
import com.jobapp.abdelkader.jobapp.data.repository.OfferFeedRepository;
import com.jobapp.abdelkader.jobapp.data.source.FeedDataSource;
import com.jobapp.abdelkader.jobapp.data.source.local.CandidateFeedLocalDataSource;
import com.jobapp.abdelkader.jobapp.data.source.local.OfferFeedLocalDataSource;
import com.jobapp.abdelkader.jobapp.jobfeed.JobFeedActivity;
import com.jobapp.abdelkader.jobapp.jobfeed.JobFeedViewModel;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user on 23/02/2017.
 */
@Module
public class JobFeedModelViewModule {
    public JobFeedModelViewModule() {
    }

    @Provides
    @Named("offerFeedRepository")
    @Singleton
    FeedRepository<OfferLine> providesOfferFeedRepository(){
        return new OfferFeedRepository();
    }

    @Provides
    @Named("candidateFeedRepository")
    @Singleton
    FeedRepository<CandidateLine> providesCandidateFeedRepository(){
        return new CandidateFeedRepository();
    }

    @Provides
    @Singleton
    JobFeedViewModel providesJobFeedViewModel(@Named("candidateFeedRepository") FeedRepository<CandidateLine> candidateFeedRepository,@Named("offerFeedRepository") FeedRepository<OfferLine> offerFeedRepository) {
        return new JobFeedViewModel(candidateFeedRepository,offerFeedRepository);
    }
}
