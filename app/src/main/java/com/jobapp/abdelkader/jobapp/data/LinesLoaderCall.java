package com.jobapp.abdelkader.jobapp.data;

import com.jobapp.abdelkader.jobapp.data.model.Line;
import com.jobapp.abdelkader.jobapp.data.model.OfferLine;

import java.util.Collection;
import java.util.List;

/**
 * Created by user on 15/02/2017.
 */
public interface LinesLoaderCall<T> {
    void onSuccesCallback(List<T> lines);
    void onFailureCallback();
}
