package com.jobapp.abdelkader.jobapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by Abdelkader on 12/02/2017.
 */

public final class CandidateLine extends Line{
    @SerializedName("id")
    private final int _id;
    private final String firstName;
    private final String lastName;
    private final int age;
    private final int experience;
    private final String job;
    private final List<String> tags;
    private final int currentRecommendation;
    private final boolean isLocallyRocommended;

    public CandidateLine(int _id, String firstName, String lastName, int age, int experience, String job, List<String> tags, int currentRecommendation,String postedDate, String picture,boolean isLocallyRocommended) {
        /*2016-04-23 08:21:59,"https://unsplash.it/100/100/?image=1",1354,"Munoz","Harding",35,4,"voluptate amet",Collections.EMPTY_LIST,23*/

        super(postedDate, picture);
        this._id = _id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.experience = experience;
        this.job = job;
        this.tags = tags;
        this.currentRecommendation = currentRecommendation;
        this.isLocallyRocommended=isLocallyRocommended;
    }

    public int get_id() {
        return _id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public int getExperience() {
        return experience;
    }

    public String getJob() {
        return job;
    }

    public List<String> getTags() {
        return tags;
    }

    // TODO : use formated String
    public int getCurrentRecommendation() {
        return currentRecommendation;
    }

    @Override
    public String getPostedDate() {
        return super.getPostedDate();
    }

    @Override
    public String getPicture() {
        return super.getPicture();
    }

    public boolean isLocallyRocommended() {
        return isLocallyRocommended;
    }
}
