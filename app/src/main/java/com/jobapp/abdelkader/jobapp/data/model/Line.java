package com.jobapp.abdelkader.jobapp.data.model;

import java.util.Date;

/**
 * Created by user on 14/02/2017.
 */

public abstract class Line implements Comparable<Line>{
    protected final String postedDate;
    protected final String picture;

    Line(String postedDate, String picture) {
        this.postedDate = postedDate;
        this.picture = picture;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public String getPicture() {
        return picture;
    }

    @Override
    public int compareTo(Line line) {
        return this.postedDate.compareTo(line.getPostedDate());
    }
}
