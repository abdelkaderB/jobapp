package com.jobapp.abdelkader.jobapp.data.model;

import java.util.Date;

/**
 * Created by Abdelkader on 12/02/2017.
 "viewedCount": 32,
 "submittedCount": 27,
 }
 */

public final class OfferLine extends Line {
    private final String title;
    private final String postedBy;
    private final int viewedCount;
    private final int submittedCount;

    public OfferLine(String title, String postedBy, int viewedCount, int submittedCount, String postedDate, String picture) {
        /*"duis consectetur id irure veniam","Opportech",32,27,2016-04-23,"https://unsplash.it/100/100/?image=1"*/
        super(postedDate, picture);
        this.title = title;
        this.postedBy = postedBy;
        this.viewedCount = viewedCount;
        this.submittedCount = submittedCount;
    }

    public String getTitle() {
        return title;
    }

    public String getPostedBy() {
        return postedBy;
    }

    public int getViewedCount() {
        return viewedCount;
    }

    public int getSubmittedCount() {
        return submittedCount;
    }

    @Override
    public String getPostedDate() {
        return super.getPostedDate();
    }

    @Override
    public String getPicture() {
        return super.getPicture();
    }
}
