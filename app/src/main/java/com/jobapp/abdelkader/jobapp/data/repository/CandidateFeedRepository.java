package com.jobapp.abdelkader.jobapp.data.repository;

import android.util.Log;

import com.jobapp.abdelkader.jobapp.Utils.ConnectivityHelper;
import com.jobapp.abdelkader.jobapp.data.LinesLoaderCall;
import com.jobapp.abdelkader.jobapp.data.model.CandidateLine;
import com.jobapp.abdelkader.jobapp.data.source.FeedDataSource;
import com.jobapp.abdelkader.jobapp.data.source.local.CandidateFeedLocalDataSource;
import com.jobapp.abdelkader.jobapp.data.source.remote.CandidateFeedRemoteDataSource;

import java.util.List;

/**
 * Created by user on 15/02/2017.
 */

public final class CandidateFeedRepository implements FeedRepository<CandidateLine> {
    private static final String TAG = CandidateFeedRepository.class.getCanonicalName();
    private final FeedDataSource<CandidateLine> candidateLocalFeedDataSource;
    private final FeedDataSource<CandidateLine> candidateRemoteFeedDataSource;

    public CandidateFeedRepository() {
        String url="http://beta.json-generator.com";
        candidateLocalFeedDataSource=new CandidateFeedLocalDataSource();
        candidateRemoteFeedDataSource=new CandidateFeedRemoteDataSource(url);
    }

    public CandidateFeedRepository(FeedDataSource<CandidateLine> candidateLocalFeedDataSource, FeedDataSource<CandidateLine> candidateRemoteFeedDataSource) {
        this.candidateLocalFeedDataSource = candidateLocalFeedDataSource;
        this.candidateRemoteFeedDataSource = candidateRemoteFeedDataSource;
    }
    @Override
    public void getFeedLines(final LinesLoaderCall<CandidateLine> linesLoaderCallBack) {
        if(isConnectedToInternet())
            candidateRemoteFeedDataSource.getLines(new LinesLoaderCall<CandidateLine>() {
                @Override
                public void onSuccesCallback(List<CandidateLine> lines) {
                    linesLoaderCallBack.onSuccesCallback(lines);
                    if (candidateLocalFeedDataSource instanceof CandidateFeedLocalDataSource)
                        ((CandidateFeedLocalDataSource) candidateLocalFeedDataSource).replaceAllLinesWith(lines);
                }
                @Override
                public void onFailureCallback() {
                    Log.e(TAG, "local candidate lines replaced operation failed");
                }
            });
        else{
            candidateLocalFeedDataSource.getLines(linesLoaderCallBack);
        }
    }

    private boolean isConnectedToInternet() {
        return ConnectivityHelper.isConnectedToInternet();
    }

    public boolean addLocalCandidateRocommandation(int candidateId){
        if (candidateLocalFeedDataSource instanceof CandidateFeedLocalDataSource)
            return ((CandidateFeedLocalDataSource) candidateLocalFeedDataSource).updateCandidateRocommandation(candidateId);
        return false;
    }
}
