package com.jobapp.abdelkader.jobapp.data.repository;

import com.jobapp.abdelkader.jobapp.data.LinesLoaderCall;
import com.jobapp.abdelkader.jobapp.data.model.Line;

/**
 * Created by user on 15/02/2017.
 */

public interface FeedRepository<T extends Line> {
    public void getFeedLines(LinesLoaderCall<T> linesLoaderCallBack);
}
