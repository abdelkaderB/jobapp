package com.jobapp.abdelkader.jobapp.data.repository;

import android.util.Log;

import com.jobapp.abdelkader.jobapp.Utils.ConnectivityHelper;
import com.jobapp.abdelkader.jobapp.data.LinesLoaderCall;
import com.jobapp.abdelkader.jobapp.data.model.OfferLine;
import com.jobapp.abdelkader.jobapp.data.source.FeedDataSource;
import com.jobapp.abdelkader.jobapp.data.source.local.CandidateFeedLocalDataSource;
import com.jobapp.abdelkader.jobapp.data.source.local.OfferFeedLocalDataSource;
import com.jobapp.abdelkader.jobapp.data.source.remote.OfferFeedRemoteDataSource;

import java.util.List;

/**
 * Created by user on 15/02/2017.
 */

public final class OfferFeedRepository implements FeedRepository<OfferLine>{
    private final static String TAG=OfferFeedRepository.class.getSimpleName();
    private FeedDataSource<OfferLine> offerFeedRemoteDataSource;
    private FeedDataSource<OfferLine> offerFeedLocalDataSource;

    public OfferFeedRepository() {
        String url="http://beta.json-generator.com";
        offerFeedRemoteDataSource=new OfferFeedRemoteDataSource(url);
        offerFeedLocalDataSource=new OfferFeedLocalDataSource();
    }

    public OfferFeedRepository(FeedDataSource<OfferLine> offerFeedRemoteDataSource, FeedDataSource<OfferLine> offerFeedLocalDataSource) {
        this.offerFeedRemoteDataSource = offerFeedRemoteDataSource;
        this.offerFeedLocalDataSource = offerFeedLocalDataSource;
    }

    @Override
    public void getFeedLines(final LinesLoaderCall<OfferLine> linesLoaderCallBack) {
        if(ConnectivityHelper.isConnectedToInternet()){
            offerFeedRemoteDataSource.getLines(new LinesLoaderCall<OfferLine>() {
                @Override
                public void onSuccesCallback(List<OfferLine> lines) {
                    linesLoaderCallBack.onSuccesCallback(lines);
                    if(offerFeedLocalDataSource instanceof OfferFeedLocalDataSource)
                        ((OfferFeedLocalDataSource)offerFeedLocalDataSource).replaceAllLinesWith(lines);
                }
                @Override
                public void onFailureCallback() {

                }
            });
        }else{
            offerFeedLocalDataSource.getLines(linesLoaderCallBack);
        }
    }
}
