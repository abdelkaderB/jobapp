package com.jobapp.abdelkader.jobapp.data.source;

import com.jobapp.abdelkader.jobapp.data.LinesLoaderCall;

import java.util.List;

/**
 * Created by user on 15/02/2017.
 */
public interface FeedDataSource<T> {
    void getLines(LinesLoaderCall<T> linesLoaderCallBack);
}
