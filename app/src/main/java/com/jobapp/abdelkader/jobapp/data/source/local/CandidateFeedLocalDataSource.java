package com.jobapp.abdelkader.jobapp.data.source.local;

import static com.jobapp.abdelkader.jobapp.data.source.local.contract.JobFeedContract.Candidate;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.jobapp.abdelkader.jobapp.JobApp;
import com.jobapp.abdelkader.jobapp.data.LinesLoaderCall;
import com.jobapp.abdelkader.jobapp.data.model.CandidateLine;
import com.jobapp.abdelkader.jobapp.data.source.FeedDataSource;
import com.jobapp.abdelkader.jobapp.data.source.local.contract.JobFeedContract;
import com.jobapp.abdelkader.jobapp.data.source.local.contract.JobFeedDbHelper;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by user on 15/02/2017.
 */

public final class CandidateFeedLocalDataSource implements FeedDataSource<CandidateLine>,Closeable {
    private static final String TAG = CandidateFeedLocalDataSource.class.getCanonicalName();

    private final JobFeedDbHelper jobFeedDbHelper;

    public CandidateFeedLocalDataSource() {
        jobFeedDbHelper = new JobFeedDbHelper(JobApp.getContext());
    }

    @Override
    public void getLines(final LinesLoaderCall<CandidateLine> linesLoaderCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SQLiteDatabase jobFeedDB =jobFeedDbHelper.getReadableDatabase();
                final Cursor cursor = jobFeedDB.query(
                        JobFeedContract.Candidate.TABLE_NAME,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                );
                List<CandidateLine> candidateLines = converteToLignes(cursor);
                cursor.close();
                linesLoaderCallBack.onSuccesCallback(candidateLines);
                Log.i(TAG,"fetching ("+candidateLines.size()+") candidate lines from localDB");
          }
        }).start();
    }

    private List<CandidateLine> converteToLignes(Cursor cursor) {
        final List<CandidateLine> candidateLines=new ArrayList<>();
        while(cursor.moveToNext()){
            CandidateLine candidateLine=converteToLigne(cursor);
            candidateLines.add(candidateLine);
        }
        return candidateLines;
    }

    private CandidateLine converteToLigne(final Cursor cursor) {
        final int _id=cursor.getInt(cursor.getColumnIndex(Candidate.COLUMN_ID));
        final String firstName=cursor.getString(cursor.getColumnIndex(Candidate.COLUMN_FIRST_NAME));
        final String lastName=cursor.getString(cursor.getColumnIndex(Candidate.COLUMN_FIRST_NAME));
        final int age=cursor.getInt(cursor.getColumnIndex(Candidate.COLUMN_AGE));
        final int experience=cursor.getInt(cursor.getColumnIndex(Candidate.COLUMN_EXPERIENCE));
        final String job=cursor.getString(cursor.getColumnIndex(Candidate.COLUMN_JOB));
        final List<String> tags= Arrays.asList(cursor.getString(cursor.getColumnIndex(Candidate.COLUMN_TAGS)).split(","));
        final int currentRecommendation=cursor.getInt(cursor.getColumnIndex(Candidate.COLUMN_CURRENT_RECOMMENDATION));
        final String picture=cursor.getString(cursor.getColumnIndex(Candidate.COLUMN_PICTURE));
        final String posteDate=cursor.getString(cursor.getColumnIndex(Candidate.COLUMN_POSTE_DATE));
        final boolean isLocallyRocommended=cursor.getInt(cursor.getColumnIndex(Candidate.COLUMN_LOCAL_RECOMMENDATION))==0?false:true;
        return new CandidateLine(_id,firstName,lastName,age,experience,job,null,currentRecommendation,picture,posteDate,isLocallyRocommended);
    }

    public void replaceAllLinesWith(final List<CandidateLine> lines) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SQLiteDatabase jobFeedDB =jobFeedDbHelper.getWritableDatabase();
                jobFeedDB.delete(Candidate.TABLE_NAME,null,null);
                for (int i = 0; i < lines.size(); i++) {
                    saveLine(lines.get(i),jobFeedDB);
                }
                Log.i(TAG,"replacing ("+lines.size()+") candidate lines in localDB");
            }
        }).start();
    }

    private long saveLine(CandidateLine candidate,SQLiteDatabase jobFeedDB) {
        ContentValues values = new ContentValues();
        values.put(Candidate.COLUMN_ID, candidate.get_id());
        values.put(Candidate.COLUMN_FIRST_NAME, candidate.getFirstName());
        values.put(Candidate.COLUMN_LAST_NAME, candidate.getLastName());
        values.put(Candidate.COLUMN_JOB, candidate.getJob());
        values.put(Candidate.COLUMN_AGE, candidate.getAge());
        values.put(Candidate.COLUMN_EXPERIENCE, candidate.getExperience());
        values.put(Candidate.COLUMN_CURRENT_RECOMMENDATION, candidate.getCurrentRecommendation());
        values.put(Candidate.COLUMN_TAGS, String.valueOf(candidate.getTags()));
        values.put(Candidate.COLUMN_PICTURE, candidate.getPicture());
        values.put(Candidate.COLUMN_POSTE_DATE, candidate.getPostedDate());
        values.put(Candidate.COLUMN_LOCAL_RECOMMENDATION, 0);
        return jobFeedDB.insert(Candidate.TABLE_NAME, null, values);
    }
    public boolean updateCandidateRocommandation(int candidateId){
        SQLiteDatabase jobFeedDB =jobFeedDbHelper.getWritableDatabase();
        final ContentValues values = new ContentValues();
        values.put(Candidate.COLUMN_LOCAL_RECOMMENDATION, 1);
        int update = jobFeedDB.update(Candidate.TABLE_NAME, values, Candidate.COLUMN_ID + " = "+candidateId, null);
        Log.i(TAG,"succesful rocommandation of candidate "+candidateId+" in localDB");
        return update>0;
    }

    @Override
    public void close() {
        jobFeedDbHelper.close();
    }
}
