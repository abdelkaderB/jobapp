package com.jobapp.abdelkader.jobapp.data.source.local;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.jobapp.abdelkader.jobapp.JobApp;
import com.jobapp.abdelkader.jobapp.data.LinesLoaderCall;
import com.jobapp.abdelkader.jobapp.data.model.OfferLine;
import com.jobapp.abdelkader.jobapp.data.source.FeedDataSource;
import com.jobapp.abdelkader.jobapp.data.source.local.contract.JobFeedContract;
import com.jobapp.abdelkader.jobapp.data.source.local.contract.JobFeedDbHelper;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 15/02/2017.
 */

public final class OfferFeedLocalDataSource implements FeedDataSource<OfferLine>{
    public static final String TAG = OfferFeedLocalDataSource.class.getCanonicalName();
    final JobFeedDbHelper jobFeedDbHelper;

    public OfferFeedLocalDataSource() {
        jobFeedDbHelper = new JobFeedDbHelper(JobApp.getContext());
    }

    @Override
    public void getLines(final LinesLoaderCall<OfferLine> linesLoaderCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SQLiteDatabase jobFeedDB =jobFeedDbHelper.getReadableDatabase();
                final Cursor cursor = jobFeedDB.query(
                        JobFeedContract.Offer.TABLE_NAME,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                );
                List<OfferLine> offerLines = converteToLignes(cursor);
                cursor.close();
                linesLoaderCallBack.onSuccesCallback(offerLines);
                Log.i(TAG,"fetching ("+offerLines.size()+") offer lines from localDB");
            }}).start();
    }

    private List<OfferLine> converteToLignes(Cursor cursor) {
        final List<OfferLine> candidateLines=new ArrayList<>();
        while(cursor.moveToNext()){
            OfferLine candidateLine=converteToLigne(cursor);
            candidateLines.add(candidateLine);
        }
        return candidateLines;
    }

    private OfferLine converteToLigne(final Cursor cursor) {
        final String title=cursor.getString(cursor.getColumnIndex(JobFeedContract.Offer.COLUMN_TITLE));
        final String postedBy=cursor.getString(cursor.getColumnIndex(JobFeedContract.Offer.COLUMN_POSTED_BY));
        final String postedDate=cursor.getString(cursor.getColumnIndex(JobFeedContract.Offer.COLUMN_POSTE_DATE));
        final int viewedCount=cursor.getInt(cursor.getColumnIndex(JobFeedContract.Offer.COLUMN_VIEWED_COUNT));
        final int submitedCount=cursor.getInt(cursor.getColumnIndex(JobFeedContract.Offer.COLUMN_SUBMITTED_COUNT));
        final String picture=cursor.getString(cursor.getColumnIndex(JobFeedContract.Offer.COLUMN_PICTURE));
        return new OfferLine(title,postedBy,viewedCount,submitedCount,postedDate,picture);
    }
    public void replaceAllLinesWith(final List<OfferLine> lines) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SQLiteDatabase jobFeedDB =jobFeedDbHelper.getWritableDatabase();
                jobFeedDB.delete(JobFeedContract.Offer.TABLE_NAME,null,null);
                for (int i = 0; i < lines.size(); i++) {
                    saveLine(lines.get(i), jobFeedDB);
                }
                Log.i(TAG,"replacing ("+lines.size()+") offer lines in localDB");
            }}).start();
    }

    private long saveLine(OfferLine offerLine,SQLiteDatabase jobFeedDB) {
        ContentValues values = new ContentValues();
        values.put(JobFeedContract.Offer.COLUMN_TITLE,offerLine.getTitle());
        values.put(JobFeedContract.Offer.COLUMN_POSTED_BY,offerLine.getPostedBy());
        values.put(JobFeedContract.Offer.COLUMN_POSTE_DATE,offerLine.getPostedDate());
        values.put(JobFeedContract.Offer.COLUMN_VIEWED_COUNT,offerLine.getViewedCount());
        values.put(JobFeedContract.Offer.COLUMN_SUBMITTED_COUNT,offerLine.getSubmittedCount());
        values.put(JobFeedContract.Offer.COLUMN_PICTURE,offerLine.getPicture());
        return jobFeedDB.insert(JobFeedContract.Offer.TABLE_NAME, null, values);
    }
}
