package com.jobapp.abdelkader.jobapp.data.source.local.contract;

import android.provider.BaseColumns;

/**
 * Created by Abdelkader on 18/02/2017.
 */

public final class JobFeedContract {

    public static final class Candidate implements BaseColumns{
        public static final String TABLE_NAME="candidate";
        public static final String COLUMN_ID="Id";
        public static final String COLUMN_FIRST_NAME="firstName";
        public static final String COLUMN_LAST_NAME = "lastName";
        public static final String COLUMN_AGE="age";
        public static final String COLUMN_EXPERIENCE="experience";
        public static final String COLUMN_JOB="job";
        public static final String COLUMN_TAGS="tags";
        public static final String COLUMN_CURRENT_RECOMMENDATION="currentRecommendation";
        public static final String COLUMN_LOCAL_RECOMMENDATION="localRecommendation";
        public static final String COLUMN_PICTURE="picture";
        public static final String COLUMN_POSTE_DATE="postedDate";
    }

    public static final class Offer implements BaseColumns{
        public static final String TABLE_NAME="offer";
        public static final String COLUMN_TITLE="title";
        public static final String COLUMN_POSTED_BY= "postedBy";
        public static final String COLUMN_VIEWED_COUNT="viewedCount";
        public static final String COLUMN_SUBMITTED_COUNT="submittedCount";
        public static final String COLUMN_PICTURE="picture";
        public static final String COLUMN_POSTE_DATE="postedDate";
    }
}
