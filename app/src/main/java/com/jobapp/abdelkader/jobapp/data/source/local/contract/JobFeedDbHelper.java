package com.jobapp.abdelkader.jobapp.data.source.local.contract;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Abdelkader on 18/02/2017.
 */

public class JobFeedDbHelper extends SQLiteOpenHelper {
    private static String DATABASE_NAME="JOB_FEED.db";
    private static int DATABASE_VESRION=1;
    private SQLiteDatabase readableDatabase;
    private SQLiteDatabase writableDatabase;
    public JobFeedDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VESRION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String candidateDBcreationQuery="CREATE TABLE "+
                JobFeedContract.Candidate.TABLE_NAME +"("+
                JobFeedContract.Candidate._ID+ " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                JobFeedContract.Candidate.COLUMN_ID+ " INTEGER NOT NULL,"+
                JobFeedContract.Candidate.COLUMN_FIRST_NAME+ " TEXT NOT NULL,"+
                JobFeedContract.Candidate.COLUMN_LAST_NAME + " TEXT NOT NULL,"+
                JobFeedContract.Candidate.COLUMN_JOB+ " TEXT NOT NULL,"+
                JobFeedContract.Candidate.COLUMN_EXPERIENCE+ " TEXT NOT NULL,"+
                JobFeedContract.Candidate.COLUMN_PICTURE+ " TEXT NOT NULL,"+
                JobFeedContract.Candidate.COLUMN_AGE+ " INTEGER NOT NULL,"+
                JobFeedContract.Candidate.COLUMN_TAGS+ "  TEXT,"+
                JobFeedContract.Candidate.COLUMN_CURRENT_RECOMMENDATION+ " INTEGER NOT NULL,"+
                JobFeedContract.Candidate.COLUMN_LOCAL_RECOMMENDATION+ " INTEGER,"+ //CHECK ("+JobFeedContract.Candidate.COLUMN_LOCAL_RECOMMENDATION+" IN (0,1))
                JobFeedContract.Candidate.COLUMN_POSTE_DATE+ " DATE NOT NULL" +
                ");";

        final String offerDBcreationQuery="CREATE TABLE "+
                JobFeedContract.Offer.TABLE_NAME +"("+
                JobFeedContract.Offer._ID+ " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                JobFeedContract.Offer.COLUMN_TITLE+ " TEXT NOT NULL,"+
                JobFeedContract.Offer.COLUMN_POSTED_BY + " TEXT NOT NULL,"+
                JobFeedContract.Offer.COLUMN_SUBMITTED_COUNT+ " INTEGER NOT NULL,"+
                JobFeedContract.Offer.COLUMN_VIEWED_COUNT+ " INTEGER NOT NULL,"+
                JobFeedContract.Offer.COLUMN_POSTE_DATE+ " DATE NOT NULL," +
                JobFeedContract.Offer.COLUMN_PICTURE+ " TEXT NOT NULL"+
                ");";
        db.execSQL(candidateDBcreationQuery);
        db.execSQL(offerDBcreationQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + JobFeedContract.Candidate.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + JobFeedContract.Offer.TABLE_NAME);
        onCreate(db);
    }
    @Override
    public synchronized SQLiteDatabase getReadableDatabase(){
        if(readableDatabase==null)
            readableDatabase=super.getReadableDatabase();
        return readableDatabase;
    }

    @Override
    public synchronized SQLiteDatabase getWritableDatabase(){
        if(writableDatabase==null)
            writableDatabase=super.getWritableDatabase();
        return writableDatabase;
    }

    @Override
    public void close() {
        if(writableDatabase!=null)
            writableDatabase.close();
        if(readableDatabase!=null)
            readableDatabase.close();

        this.close();
    }
}
