package com.jobapp.abdelkader.jobapp.data.source.remote;

import android.util.Log;

import com.google.gson.Gson;
import com.jobapp.abdelkader.jobapp.data.LinesLoaderCall;
import com.jobapp.abdelkader.jobapp.data.model.CandidateLine;
import com.jobapp.abdelkader.jobapp.data.source.FeedDataSource;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by user on 15/02/2017.
 */

public final class CandidateFeedRemoteDataSource implements FeedDataSource<CandidateLine> {
    public static final String TAG = CandidateFeedRemoteDataSource.class.getCanonicalName();
    private final String url;
    public CandidateFeedRemoteDataSource(String url) {
        this.url = url;
    }
    @Override
    public void getLines(final LinesLoaderCall<CandidateLine> linesLoaderCallBack) {
        Call<List<CandidateLine>> listCandidateLines = buildServiceCall();
        listCandidateLines.enqueue(new Callback<List<CandidateLine>>() {
            @Override
            public void onResponse(Call<List<CandidateLine>> call, Response<List<CandidateLine>> response) {
                linesLoaderCallBack.onSuccesCallback(response.body());
                Log.i(TAG,"fetching ("+response.body().size()+") candidate lines remotly.");
            }

            @Override
            public void onFailure(Call<List<CandidateLine>> call, Throwable t) {
                linesLoaderCallBack.onFailureCallback();
                Log.e(TAG,"Failed to make a remote call.");
            }
        });
    }

    private Call<List<CandidateLine>> buildServiceCall() {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(url)
                .build();
        CandidateService service = retrofit.create(CandidateService.class);
        return service.listCandidateLines();
    }

    private interface CandidateService {
        @GET("/api/json/get/EJ5uiN98b")
        Call<List<CandidateLine>> listCandidateLines();
    }
}
