package com.jobapp.abdelkader.jobapp.data.source.remote;

import android.util.Log;

import com.jobapp.abdelkader.jobapp.data.LinesLoaderCall;
import com.jobapp.abdelkader.jobapp.data.model.OfferLine;
import com.jobapp.abdelkader.jobapp.data.source.FeedDataSource;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by user on 15/02/2017.
 */
//to be refactored
public final class OfferFeedRemoteDataSource implements FeedDataSource<OfferLine> {
    private static final String TAG = OfferFeedRemoteDataSource.class.getCanonicalName();
    private final String url;
    public OfferFeedRemoteDataSource(String url) {
        this.url = url;
    }
    @Override
    public void getLines(final LinesLoaderCall<OfferLine> linesLoaderCallBack) {
        Call<List<OfferLine>> listOfferLines = buildServiceCall();
        listOfferLines.enqueue(new Callback<List<OfferLine>>() {
            @Override
            public void onResponse(Call<List<OfferLine>> call, Response<List<OfferLine>> response) {
                linesLoaderCallBack.onSuccesCallback(response.body());
                Log.i(TAG,"fetching ("+response.body().size()+") offer lines remotly.");
            }

            @Override
            public void onFailure(Call<List<OfferLine>> call, Throwable t) {
                linesLoaderCallBack.onFailureCallback();
                Log.e(TAG,"Failed to make a remote call.");
            }
        });
    }

    private Call<List<OfferLine>> buildServiceCall() {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(url)
                .build();
        OfferService service = retrofit.create(OfferService.class);
        return service.listOfferLines();
    }

    private interface OfferService {
        @GET("/api/json/get/41OUsE5UW")
        Call<List<OfferLine>> listOfferLines();
    }
}
