package com.jobapp.abdelkader.jobapp.jobfeed;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.jobapp.abdelkader.jobapp.JobApp;
import com.jobapp.abdelkader.jobapp.R;
import com.jobapp.abdelkader.jobapp.data.model.CandidateLine;
import com.jobapp.abdelkader.jobapp.data.model.Line;
import com.jobapp.abdelkader.jobapp.receiver.HeadSetBroadCastReceiver;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class JobFeedActivity extends AppCompatActivity{
    public static final int LOCAL_ROCOMMENDATION_LOADER=0;
    public HeadSetBroadCastReceiver headsetReceiver;
    private RecyclerView mRecyclerView;
    @Inject JobFeedViewModel jobFeedViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_feed);

        ((JobApp)getApplication()).getJobFeedViewModelComponent().inject(this);
        headsetReceiver = new HeadSetBroadCastReceiver();
        mRecyclerView = (RecyclerView) findViewById(R.id.feed_recycle_view);
        mRecyclerView.setHasFixedSize(true);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        final JobFeedActivity jobFeedActivity=this;
        JobFeedAdapter mAdapter= new JobFeedAdapter(new ArrayList<Line>(), getMyAdapterListener());
        mRecyclerView.setAdapter(mAdapter);
        jobFeedViewModel.loadFeed(this);
    }

    @NonNull
    private JobFeedAdapter.MyAdapterListener getMyAdapterListener() {
        return new JobFeedAdapter.MyAdapterListener() {
            @Override
            public void rocommondationOnClickListener(final View view, final CandidateLine candidate) {
                Log.i("rocomm","rocommondationOnClickListener"+candidate);
                AsyncTask<CandidateLine, Integer, Boolean> asyncTasknew = new AsyncTask<CandidateLine, Integer, Boolean>() {
                    @Override
                    protected void onPreExecute(){
                        view.animate().alpha(0.5f).start();
                        Log.i("onPreExecute","onPreExecute");
                    }
                    @Override
                    protected Boolean doInBackground(CandidateLine... params) {
                        Integer condidateId =params[0].get_id();
                        Log.i("doInBackground",params[0].toString());
                        // to simulate latency
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return jobFeedViewModel.updateRocommondationfor(condidateId);
                    }
                    @Override
                    protected void onPostExecute(Boolean result){
                        view.animate().alpha(1f).start();
                        Log.i("onPostExecute","onPostExecute");
                    }
                };
                asyncTasknew.execute(candidate);
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter headsetPlugIntentFilter=new IntentFilter();
        headsetPlugIntentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(headsetReceiver,headsetPlugIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(headsetReceiver);
    }

    public void pushLineInFeedJobAdapter(List<? extends Line> feedLines){
        ((JobFeedAdapter) mRecyclerView.getAdapter()).addFeedLines(feedLines);
    }
}
