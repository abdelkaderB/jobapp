package com.jobapp.abdelkader.jobapp.jobfeed;

import android.util.Log;

import com.jobapp.abdelkader.jobapp.JobApp;
import com.jobapp.abdelkader.jobapp.data.LinesLoaderCall;
import com.jobapp.abdelkader.jobapp.data.model.CandidateLine;
import com.jobapp.abdelkader.jobapp.data.model.Line;
import com.jobapp.abdelkader.jobapp.data.model.OfferLine;
import com.jobapp.abdelkader.jobapp.data.repository.CandidateFeedRepository;
import com.jobapp.abdelkader.jobapp.data.repository.FeedRepository;
import com.jobapp.abdelkader.jobapp.data.repository.OfferFeedRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by user on 15/02/2017.
 */

public class JobFeedViewModel {
    @Inject @Named("candidateFeedRepository") FeedRepository<CandidateLine> candidateFeedRepository;
    @Inject @Named("offerFeedRepository") FeedRepository<OfferLine> offerFeedRepository;
    private JobFeedActivity jobFeedActivity;

    public JobFeedViewModel(FeedRepository<CandidateLine> candidateFeedRepository, FeedRepository<OfferLine> offerFeedRepository) {
        this.candidateFeedRepository=candidateFeedRepository;
        this.offerFeedRepository=offerFeedRepository;
    }


    void loadFeed(final JobFeedActivity jobFeedActivity) {
      candidateFeedRepository.getFeedLines(new LinesLoaderCall<CandidateLine>() {
            @Override
            public void onSuccesCallback(List<CandidateLine> lines) { addLinesAndRedfreshView(lines, jobFeedActivity); }
            @Override
            public void onFailureCallback() { Log.e("CANDIDATE_REPO","faild to fetch candidate lines."); }
        });

        offerFeedRepository.getFeedLines(new LinesLoaderCall<OfferLine>() {
            @Override
            public void onSuccesCallback(List<OfferLine> lines) {
                addLinesAndRedfreshView(lines,jobFeedActivity);
            }
            @Override
            public void onFailureCallback() {
                Log.e("OFFER_REPO","faild to fetch offer lines.");
            }
        });
    }

    private void addLinesAndRedfreshView(List<? extends Line> lines, JobFeedActivity jobFeedActivity) {
        jobFeedActivity.pushLineInFeedJobAdapter(lines);
    }

    public Boolean updateRocommondationfor(int condidateId) {
        return ((CandidateFeedRepository)candidateFeedRepository).addLocalCandidateRocommandation(condidateId);
    }
}
