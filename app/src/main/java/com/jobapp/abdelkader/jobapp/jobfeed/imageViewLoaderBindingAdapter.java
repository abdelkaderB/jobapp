package com.jobapp.abdelkader.jobapp.jobfeed;

import android.databinding.BindingAdapter;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by user on 03/03/2017.
 */

public final class imageViewLoaderBindingAdapter {
    @BindingAdapter("bind:picture")
    public static void loadPicture(ImageView view, String imageUrl) {
        Picasso.with(view.getContext())
                .load(imageUrl)
                .into(view);
    }
}
