package com.jobapp.abdelkader.jobapp.jobfeed;

import android.databinding.BindingAdapter;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jobapp.abdelkader.jobapp.BR;
import com.jobapp.abdelkader.jobapp.data.model.CandidateLine;
import com.jobapp.abdelkader.jobapp.data.model.Line;
import com.jobapp.abdelkader.jobapp.data.model.OfferLine;
import com.jobapp.abdelkader.jobapp.databinding.CandidateCardItemBinding;
import com.jobapp.abdelkader.jobapp.databinding.OfferCardItemBinding;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by Abdelkader on 13/02/2017.
 */

public class JobFeedAdapter <T extends Line> extends RecyclerView.Adapter<JobFeedAdapter.LineViewHolder> {
    private static final int OFFER_LINE = 0;
    private static final int CANDIDATE_LINE = 1;


    public interface MyAdapterListener {
        void rocommondationOnClickListener(View v, CandidateLine candidate);
    }


    private List<T> feedLines;
    // TODO: 03/03/2017 rename
    private final MyAdapterListener myAdapterListener;
    JobFeedAdapter(List<T> feedLines, MyAdapterListener myAdapterListener) {
        this.feedLines = feedLines;
        this.myAdapterListener=myAdapterListener;
    }
    @Override
    public int getItemViewType(int position) {
        T line = feedLines.get(position);
        return line instanceof OfferLine ? OFFER_LINE : CANDIDATE_LINE;
    }
     void addFeedLines(List<T> feedLines){
        this.feedLines.addAll(feedLines);
        Collections.sort(this.feedLines);
        this.notifyDataSetChanged();
    }
    @Override
    public LineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(OFFER_LINE==viewType){
            OfferCardItemBinding offerCardItemBinding=OfferCardItemBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
            return new LineViewHolder(offerCardItemBinding,BR.offer);
        }
        CandidateCardItemBinding candidateCardItemBinding=CandidateCardItemBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
        return new LineViewHolder(candidateCardItemBinding,BR.candidate);
    }
    @Override
    public void onBindViewHolder(LineViewHolder holder, int position) {
        T line = feedLines.get(position);
        holder.bindDataToViewHolder(line,myAdapterListener);
    }
    @Override
    public int getItemCount() {
        return feedLines.size();
    }


    public class LineViewHolder<T extends Line> extends RecyclerView.ViewHolder {
        private final ViewDataBinding lineViewDataBinding;
        private final int variableId;
        LineViewHolder(ViewDataBinding lineViewDataBinding, int variableId) {
            super(lineViewDataBinding.getRoot());
            this.lineViewDataBinding =lineViewDataBinding;
            this.variableId = variableId;
        }

        void bindDataToViewHolder(T line, MyAdapterListener myAdapterListener){
            lineViewDataBinding.setVariable(variableId,line);
            if(BR.candidate==variableId)
                lineViewDataBinding.setVariable(BR.handler,myAdapterListener);
            lineViewDataBinding.executePendingBindings();
        }
    }

}
