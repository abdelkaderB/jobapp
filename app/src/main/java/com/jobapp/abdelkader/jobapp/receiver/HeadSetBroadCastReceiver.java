package com.jobapp.abdelkader.jobapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import com.jobapp.abdelkader.jobapp.jobfeed.JobFeedActivity;

/**
 * Created by user on 07/03/2017.
 */

// to test this broadCast Receiver use this commande : adb shell am broadcast -a android.intent.action.HEADSET_PLUG
public class HeadSetBroadCastReceiver extends BroadcastReceiver {

    public static final int DEFAULT_VALUE = -1;
    public static final int PLUGGED = 1;
    private Toast hedsetPluged;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)){
            int state = intent.getIntExtra("state",DEFAULT_VALUE);
            if(state== PLUGGED)
                hedsetPluged =Toast.makeText(context,"Headset Pluged",Toast.LENGTH_SHORT);
            else
                hedsetPluged = Toast.makeText(context,"Hedset UnPluged",Toast.LENGTH_LONG);
            hedsetPluged.show();
        }
    }
}
