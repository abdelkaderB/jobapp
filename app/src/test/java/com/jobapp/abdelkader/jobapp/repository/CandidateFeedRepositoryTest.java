package com.jobapp.abdelkader.jobapp.repository;

import com.jobapp.abdelkader.jobapp.Utils.ConnectivityHelper;
import com.jobapp.abdelkader.jobapp.data.LinesLoaderCall;
import com.jobapp.abdelkader.jobapp.data.model.CandidateLine;
import com.jobapp.abdelkader.jobapp.data.repository.CandidateFeedRepository;
import com.jobapp.abdelkader.jobapp.data.source.FeedDataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by user on 27/02/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class CandidateFeedRepositoryTest {
    @Mock
    FeedDataSource<CandidateLine> candidateLocalFeedDataSource;

    @Mock
    FeedDataSource<CandidateLine> candidateRemoteFeedDataSource;
    @Mock
    LinesLoaderCall<CandidateLine> linesLoaderCallBack;

    private CandidateFeedRepository candidateFeedRepository;
    @Before
    public void setUp(){
        candidateFeedRepository=new CandidateFeedRepository(candidateLocalFeedDataSource, candidateRemoteFeedDataSource);
    }

    @Test
    public void shouldGetFeedLineFromLocalDataWhenNoConnexion(){
        Mockito.when(ConnectivityHelper.isConnectedToInternet()).thenReturn(false);
        candidateFeedRepository.getFeedLines(linesLoaderCallBack);
        Mockito.verify(candidateLocalFeedDataSource,Mockito.only()).getLines(linesLoaderCallBack);
    }

    @Test
    public void shouldGetFeedLineFromRemoteServiceAndReplaceLocalData(){

    }
}
